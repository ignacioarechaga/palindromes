package com.arechaga.palindromes.domain;

import javax.inject.Singleton;
import java.util.stream.IntStream;

@Singleton
class PalindromeChecker {

    boolean isPalindrome(final String word) {
        if (tooSmall(word)) {//This guard clause is here just for performance improvement, avoiding extra operations
            return true;
        }

        final int totalLength = word.length();
        final String lowerCasedWord = word.toLowerCase();
        final int lastIndex = totalLength - 1;
        return IntStream
                .range(0, totalLength / 2)
                .parallel()
                .allMatch(index -> lowerCasedWord.charAt(index) == lowerCasedWord.charAt(lastIndex - index));
        /*
         * By using a parallel stream, I am assuming that huge strings will be checked, if not, the parallel stream will be slower
         * than the sequential one, since it has to create the threads, allocate memory and start them, something that is going to slow the function
         */
    }

    private boolean tooSmall(final String word) {
        if (word == null) {
            /* This will not happen, but it will detect soon if a bug is introduced on the system
             * (assuming this is code that will have some maintenance/evolutions
             */
            throw new IllegalArgumentException("Failing fast: null should not be checked");
        }
        return word.length() < 2;
    }
}
