package com.arechaga.palindromes.domain;


import org.apache.commons.lang3.StringUtils;

import java.util.Objects;

public final class ExtractedPalindrome {

    static final ExtractedPalindrome NULL = new ExtractedPalindrome(StringUtils.EMPTY, -1);
    private final String palindrome;
    private final int position;


    ExtractedPalindrome(final String palindrome, final int position) {
        this.palindrome = palindrome;
        this.position = position;
    }

    public String print() {
        return String.format("Text: %s, Index: %d, Length: %d", palindrome, position, palindrome.length());
    }

    Integer size() {
        return palindrome.length();
    }

    /*
     * Since we they are meant to be stored on a set, I am assuming that two palindromes with the same word but different start positions,
     * will be equal
     */
    @Override
    public boolean equals(final Object other) {
        if (this == other) {
            return true;
        }
        if (!(other instanceof ExtractedPalindrome)) {
            return false;
        }
        final ExtractedPalindrome that = (ExtractedPalindrome) other;
        return Objects.equals(palindrome, that.palindrome);
    }

    @Override
    public int hashCode() {
        return Objects.hash(palindrome);
    }
}
