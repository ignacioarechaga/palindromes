package com.arechaga.palindromes.domain;

import com.arechaga.palindromes.PalindromesFinder;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class RightToLeftPalindromesFinder implements PalindromesFinder {

    private final PalindromeChecker palindromeChecker;

    @Inject
    public RightToLeftPalindromesFinder(final PalindromeChecker palindromeChecker) {
        this.palindromeChecker = palindromeChecker;
    }

    public ExtractedPalindromes find(String word, Integer amountOfPalindromesToFind) {
        ExtractedPalindromes extractedPalindromes = new ExtractedPalindromes(amountOfPalindromesToFind);

        for (int leftIndex = 0; leftIndex < word.length(); leftIndex++) {
            if (extractedPalindromes.minimumLengthRequired() > word.length() - leftIndex) {
                break;//Stops the execution since there is no chance of getting bigger palindromes
            }
            extractedPalindromes.add(lookForPalindromesFromIndex(word, leftIndex));
        }
        return extractedPalindromes;
    }

    /*
     * I chose to look from right to left in order to get the biggest palindrome first.
     * It might happen that a palindrome-of-palindrome such as abcddcbaabcddcba (which is two times abcddcba)
     * will take into consideration just the bigger one and not the second smaller one.
     * When the loop from the *find* function arrives to the second occurrence of abcddcba, then it will be detected,
     * but with a different index than the other one.
     * Since in the requirements, you are asking for a set (ergo no repetitions) and only taking into consideration
     * the length of it and not the index (or first occurrence, I assume that this solution is compliant with the requirements.
     */
    private ExtractedPalindrome lookForPalindromesFromIndex(final String word, final int leftIndex) {
        final char desiredChar = word.charAt(leftIndex);
        for (int rightIndex = word.lastIndexOf(desiredChar); rightIndex > leftIndex; rightIndex--) {
            final String palindromeCandidate = word.substring(leftIndex, rightIndex + 1);
            if (palindromeChecker.isPalindrome(palindromeCandidate)) {
                return new ExtractedPalindrome(palindromeCandidate, leftIndex);
            }
        }
        return ExtractedPalindrome.NULL;//Null object pattern in order to avoid dealing with null references
    }

}
