package com.arechaga.palindromes.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

public final class ExtractedPalindromes {
    private final TreeSet<ExtractedPalindrome> storedPalindromes = new TreeSet<>(Comparator.comparing(ExtractedPalindrome::size).reversed());
    private final Integer maximumAmount;

    ExtractedPalindromes(final Integer maximumAmount) {
        this.maximumAmount = maximumAmount;
    }

    void add(ExtractedPalindrome palindrome) {
        storedPalindromes.add(palindrome);
        purgeIfNeeded();
    }

    private void purgeIfNeeded() {
        /*
        The set size will be exceeded at maximum by one unit, that´s why I am just polling once. I am also deleting the excess of palindromes in order to
        avoid wasting memory in the data structure
         */
        storedPalindromes.remove(ExtractedPalindrome.NULL);
        if (storedPalindromes.size() > maximumAmount) {
            storedPalindromes.pollLast();
        }
    }

    public List<ExtractedPalindrome> getPalindromes() {
        return Collections.unmodifiableList(new ArrayList<>(storedPalindromes));
    }

    Integer minimumLengthRequired() {
        return storedPalindromes.size() == maximumAmount ? storedPalindromes.last().size() : 0;
    }
}
