package com.arechaga.palindromes;

import com.arechaga.palindromes.domain.ExtractedPalindrome;
import io.micronaut.configuration.picocli.PicocliRunner;
import picocli.CommandLine;
import picocli.CommandLine.Command;

import javax.inject.Inject;

@Command(name = "palindromes", description = "...",
        mixinStandardHelpOptions = true)
public class PalindromesCommand implements Runnable {

    @CommandLine.Parameters(index = "0", description = "Word used to look for palindromes")
    private String word;

    @CommandLine.Parameters(index = "1", description = "Maximum palindromes to look for")
    private Integer desiredAmount;

    @Inject
    private PalindromesFinder palindromesFinder;

    public static void main(String[] args) throws Exception {
        PicocliRunner.run(PalindromesCommand.class, args);
    }

    private static void toConsole(ExtractedPalindrome palindrome) {System.out.println(palindrome.print());}

    public void run() {
        palindromesFinder
                .find(word, desiredAmount)
                .getPalindromes()
                .forEach(PalindromesCommand::toConsole);
    }
}
