package com.arechaga.palindromes;

import com.arechaga.palindromes.domain.ExtractedPalindromes;

public interface PalindromesFinder {
    ExtractedPalindromes find(String word, Integer amountOfPalindromesToFind);
}
