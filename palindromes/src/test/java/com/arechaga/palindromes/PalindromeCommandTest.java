package com.arechaga.palindromes;

import io.micronaut.configuration.picocli.PicocliRunner;
import io.micronaut.context.ApplicationContext;
import io.micronaut.context.env.Environment;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.Assert.assertTrue;

public class PalindromeCommandTest {

    @Test
    public void testWithCommandLineOption() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(baos));

        try (ApplicationContext ctx = ApplicationContext.run(Environment.CLI, Environment.TEST)) {
            String[] args = new String[]{"sqrrqabccbatudefggfedvwhijkllkjihxymnnmzpop","3"};
            PicocliRunner.run(PalindromesCommand.class, ctx, args);

            // palindromes
            assertTrue(baos.toString(), baos.toString().contains("Text: hijkllkjih, Index: 23, Length: 10"));
            assertTrue(baos.toString(), baos.toString().contains("Text: defggfed, Index: 13, Length: 8"));
            assertTrue(baos.toString(), baos.toString().contains("Text: abccba, Index: 5, Length: 6"));
        }
    }
}
