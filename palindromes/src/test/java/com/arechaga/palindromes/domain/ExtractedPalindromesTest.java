package com.arechaga.palindromes.domain;

import com.arechaga.palindromes.domain.ExtractedPalindrome;
import com.arechaga.palindromes.domain.ExtractedPalindromes;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;


public class ExtractedPalindromesTest {


    @Test
    public void shouldNotExceedTheMaximumAmountOfPalindromes() {
        ExtractedPalindromes palindromes = new ExtractedPalindromes(2);
        palindromes.add(new ExtractedPalindrome("asdffdsa", 0));
        palindromes.add(new ExtractedPalindrome("asddsa", 12));
        palindromes.add(new ExtractedPalindrome("assa", 10));

        assertThat(palindromes.getPalindromes().size(), is(2));
    }

    @Test
    public void shouldKeepTheLongestOnes() {
        ExtractedPalindromes palindromes = new ExtractedPalindromes(2);
        palindromes.add(new ExtractedPalindrome("asdffdsa", 0));
        palindromes.add(new ExtractedPalindrome("asddsa", 12));
        final ExtractedPalindrome smallestPalindrome = new ExtractedPalindrome("assa", 10);
        palindromes.add(smallestPalindrome);

        assertFalse(palindromes.getPalindromes().contains(smallestPalindrome));
    }

    @Test
    public void shouldNotRequireZeroAsMinumumSizeWhenNotEnoughElementsFound() {
        ExtractedPalindromes palindromes = new ExtractedPalindromes(3);
        palindromes.add(new ExtractedPalindrome("asdffdsa", 0));
        palindromes.add(new ExtractedPalindrome("asddsa", 12));

        assertThat(palindromes.minimumLengthRequired(), is(0));
    }

    @Test
    public void shouldNotRequireTheSizeOfTheSmallestPalindromes() {
        ExtractedPalindromes palindromes = new ExtractedPalindromes(3);
        palindromes.add(new ExtractedPalindrome("asdffdsa", 0));
        palindromes.add(new ExtractedPalindrome("asddsa", 12));
        palindromes.add(new ExtractedPalindrome("assa", 10));


        assertThat(palindromes.minimumLengthRequired(), is(4));
    }
}