package com.arechaga.palindromes.domain;

import com.arechaga.palindromes.domain.PalindromeChecker;
import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;


public class PalindromeCheckerTest {

    private PalindromeChecker checker = new PalindromeChecker();

    @Test(expected = IllegalArgumentException.class)
    public void shouldNotCheckNulls() {
        checker.isPalindrome(null);
    }

    @Test
    public void emptyStringIsPalindrome() {
        assertTrue(checker.isPalindrome(StringUtils.EMPTY));
    }

    @Test
    public void singleCharacterIsPalindrome() {
        assertTrue(checker.isPalindrome("a"));
    }

    @Test
    public void shouldReturnTrueInEvenSizedPalindromes() {
        assertTrue(checker.isPalindrome("abcddcba"));
    }

    @Test
    public void shouldReturnTrueInOddSizedPalindromes() {
        assertTrue(checker.isPalindrome("abcdxdcba"));
    }

    @Test
    public void capitalLettersShouldNotAffectTheResult() {
        assertTrue(checker.isPalindrome("ABCDdcba"));
    }
}