package com.arechaga.palindromes.domain;

import com.arechaga.palindromes.domain.ExtractedPalindrome;
import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;


public class ExtractedPalindromeTest {

    private static final String DESIRED_TEXT = "Text: hijkllkjih, Index: 23, Length: 10";

    @Test
    public void shouldReturnTheDesiredText() {
        ExtractedPalindrome palindrome = new ExtractedPalindrome("hijkllkjih", 23);

        assertThat(palindrome.print(), is(DESIRED_TEXT));
    }

    @Test
    public void shouldReturnDesiredSize() {
        final String word = "hijkllkjih";
        ExtractedPalindrome palindrome = new ExtractedPalindrome(word, 0);

        assertThat(palindrome.size(), is(word.length()));
    }

    @Test
    public void shouldCompareByWordOnly() {
        ExtractedPalindrome one = new ExtractedPalindrome("hijkllkjih", 10);
        ExtractedPalindrome two = new ExtractedPalindrome("hijkllkjih", 1);

        assertThat(one, is(two));
    }
}