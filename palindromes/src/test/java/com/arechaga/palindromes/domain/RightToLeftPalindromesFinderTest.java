package com.arechaga.palindromes.domain;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


@RunWith(MockitoJUnitRunner.class)
public class RightToLeftPalindromesFinderTest {

    @Mock
    private PalindromeChecker checker;

    @InjectMocks
    private RightToLeftPalindromesFinder finder;

    @Test
    public void shouldCheckIfItsAPalindrome() {
        finder.find("abcddcba", 1);

        verify(checker).isPalindrome("abcddcba");
    }

    @Test
    public void shouldReturnPalindrome() {
        final String palindromeWord = "abcddcba";
        when(checker.isPalindrome(palindromeWord)).thenReturn(true);

        final ExtractedPalindromes extractedPalindromes = finder.find(palindromeWord, 1);

        assertThat(extractedPalindromes.getPalindromes().get(0), is(new ExtractedPalindrome(palindromeWord, 0)));
    }

    @Test
    public void shouldReturnNoPalindromesIfNoneFound() {
        final String palindromeWord = "abcddcba";

        final ExtractedPalindromes extractedPalindromes = finder.find(palindromeWord, 10);

        assertThat(extractedPalindromes.getPalindromes().size(), is(0));
    }

    @Test
    public void shouldReturnValidPalindromesOnly() {
        final String palindromeWord = "abcddcba";
        when(checker.isPalindrome(palindromeWord)).thenReturn(true);

        final ExtractedPalindromes extractedPalindromes = finder.find(palindromeWord, 10);

        assertThat(extractedPalindromes.getPalindromes().size(), is(1));
    }

    @Test
    public void shouldNotCheckSmallerPalindromes() {
        final String palindromeWord = "abcddcbazz";
        when(checker.isPalindrome("abcddcba")).thenReturn(true);

        finder.find(palindromeWord, 1);

        verify(checker, times(0)).isPalindrome("zz");
    }

    @Test
    public void shouldDetectPalindromeOfPalindromes() {
        final String palindromeWord = "abcddcbaabcddcba";
        when(checker.isPalindrome("abcddcba")).thenReturn(true);
        when(checker.isPalindrome("abcddcbaabcddcba")).thenReturn(true);

        finder.find(palindromeWord, 3);

        verify(checker, times(0)).isPalindrome("zz");
    }

}