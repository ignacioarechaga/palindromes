# Palindromes Code Test
## Author

**Ignacio Arechaga** - October 22nd, 2018


### Prerequisites

* Maven 3
* Java 9

### Installing

Go to the folder on which the `pom.xml` file is, and then execute the following command in order to build the project

```
mvn clean install
```

If you want to use the IDE of your choice, please enable the `annotation processing`. If you need help on how to do this, please visit [this website](https://docs.micronaut.io/latest/guide/index.html#ideSetup)

With the command line there is no problem, since all the configuration is included in the `pom.xml` file.
## Running the application

Once the jar is built from the previous step, go to the `target` folder and execute the following command :

```
java -jar palindromes.jar abcddcba 3
```

Where `abcddcba` is the word you want to check, and `3` is the amount of palindromes to show as a result in the console

### Tests, tests, tests

You will find unit tests for all the domain classes, using a *mockist-style* approach when dealing with collaborators (e.g. `RightToLeftPalindromesFinderTest`)
and a full integration in the `PalindromeCommandTest` that belongs more to the infrastructure rather than the domain itself.

### Assumptions

All assumptions are explained through comments in the production codebase.
I am not a fan of comments, since the code should be self-explanatory, but since I am doing a coding test, I want to make clear why I took one approach and help you see how my mind works when coding.

### Other considerations

I included one example of the Null Object Pattern just to make clear that I am not a fan of checking `null` and I always try avoid it as much as possible.


## Built With

* [Micronaut](http://micronaut.io/) - The framework used, in its RC3 version
* [Picocli](https://picocli.info/) - Used to capture the commands and run the application
* [Maven](https://maven.apache.org/) - Dependency Management
